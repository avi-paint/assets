package structure

import (
	"encoding/json"
	"log"
	"path"
	"strings"

	"github.com/Jeffail/gabs/v2"
	"github.com/pkg/errors"

	"gitlab.com/avi-paint/assets/pkg/utils"
)

//go:generate gojson -name=Assets -input structure.json -o structure.go -pkg main

type File struct {
	Name string
	Path string
}

func generateStructure() {
	jsonObj := gabs.New()
	information, _ := utils.GetListingDirectoryInfo(utils.GetProjectDir())
	ignoreFiles := utils.ScanIgnoreFile()

	for _, info := range information {
		if !utils.Contains(info.Path, ignoreFiles) {
			parsePath := utils.ParsePath(info)

			if !info.IsDir {
				hierarchy := strings.Join(parsePath[:len(parsePath)-1], ".")

				err := jsonObj.ArrayConcatP(File{
					Name: info.Name,
					Path: info.Path,
				}, hierarchy)
				if err != nil {
					_ = errors.Wrap(err, "error add information to json: assets main.go")
				}
			}
		}
	}

	jsonMap := make(map[string]interface{})
	_ = json.Unmarshal([]byte(jsonObj.String()), &jsonMap)

	d, err := json.Marshal(jsonMap)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	_ = utils.WriteFile(d, path.Join(utils.GetProjectDir(), "pkg//structure/structure.json"))
}
