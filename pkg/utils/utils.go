package utils

import (
	"bufio"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type Info struct {
	Name    string
	Path    string
	IsDir   bool
	ModTime time.Time
	Size    int64
}

func GetListingDirectoryInfo(root string) ([]Info, error) {
	information := make([]Info, 0)
	err := filepath.Walk(root,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			information = append(information, Info{
				Name:    info.Name(),
				Path:    path,
				IsDir:   info.IsDir(),
				ModTime: info.ModTime(),
				Size:    info.Size(),
			})
			return nil
		})
	if err != nil {
		log.Println(err)
	}
	return information, err
}

func WriteFile(data []byte, path string) error {
	err := ioutil.WriteFile(path, data, 0644)
	if err != nil {
		log.Println("cannot write file:", path)
	}
	return err
}

func ParsePath(info Info) []string {
	result := strings.Split(info.Path, "/")
	systemP := strings.Split(GetProjectDir(), "/")
	return result[len(systemP):]
}

func GetProjectDir() string {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return dir
}

func ScanIgnoreFile() []string {
	f, err := os.OpenFile(GetProjectDir()+"/.ignore", os.O_RDONLY, os.ModePerm)
	if err != nil {
		log.Fatalf("open file error: %v", err)
		return nil
	}
	defer f.Close()

	ignore := make([]string, 0)
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		ignore = append(ignore, sc.Text())
	}
	if err := sc.Err(); err != nil {
		log.Fatalf("scan file error: %v", err)
		return nil
	}

	return ignore
}

func Contains(str string, values []string) bool {
	for _, value := range values {
		if strings.Contains(str, value) {
			return true
		}
	}
	return false
}
