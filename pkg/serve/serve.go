// Package serve provides helpers to start and shutdown neural_network services.
package serve

import (
	"context"
	"net"

	"github.com/powerman/structlog"
	"google.golang.org/grpc"

	"gitlab.com/avi-paint/assets/pkg/def"
	"gitlab.com/avi-paint/assets/pkg/netx"
)

// Ctx is a synonym for convenience.
type Ctx = context.Context

func ServerGRPC(ctx Ctx, addr netx.Addr, srv *grpc.Server) error {
	log := structlog.FromContext(ctx, nil).New(def.LogServer, addr.String())

	listen, err := net.Listen("tcp", addr.String())
	if err != nil {
		return err
	}

	errc := make(chan error, 1)
	go func() { errc <- srv.Serve(listen) }()

	select {
	case err = <-errc:
	case <-ctx.Done():
		_ = srv.Stop
	}
	if err != nil {
		return log.Err("failed to serve grpc", "err", err)
	}
	return nil
}
