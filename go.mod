module gitlab.com/avi-paint/assets

go 1.17

require (
	github.com/Jeffail/gabs/v2 v2.6.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/pkg/errors v0.9.1
	github.com/powerman/must v0.1.1
	github.com/powerman/structlog v0.7.3
	gitlab.com/avi-paint/assets-proto v1.0.0
	google.golang.org/grpc v1.42.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

require (
	github.com/golang/protobuf v1.5.0 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4 // indirect
	golang.org/x/sys v0.0.0-20210510120138-977fb7262007 // indirect
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20200707001353-8e8330bf89df // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
