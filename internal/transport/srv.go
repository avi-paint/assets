package transport

import (
	"context"
	"time"

	middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	"github.com/powerman/structlog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"

	api "gitlab.com/avi-paint/assets-proto/go/v1/assets"
)

type service struct {
	api.UnimplementedAssetsServiceServer
}

type (
	Ctx = context.Context
	Log = *structlog.Logger
)

func NewAssetsServer() *grpc.Server {
	srv := grpc.NewServer(
		grpc.KeepaliveParams(keepalive.ServerParameters{
			Time:    50 * time.Second,
			Timeout: 10 * time.Second,
		}),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             30 * time.Second,
			PermitWithoutStream: true,
		}),
		grpc.UnaryInterceptor(middleware.ChainUnaryServer(
			unaryServerLogger,
			unaryServerRecover,
			unaryServerAccessLog,
		)),
		grpc.StreamInterceptor(middleware.ChainStreamServer(
			streamServerLogger,
			streamServerRecover,
			streamServerAccessLog,
		)),
	)

	api.RegisterAssetsServiceServer(srv, &service{})
	return srv
}
