package transport

import (
	"context"
	"fmt"

	api "gitlab.com/avi-paint/assets-proto/go/v1/assets"
)

func (s *service) GetPictureByPath(ctx context.Context, in *api.GetPictureRequest) (*api.GetPictureReply, error) {
	fmt.Printf("path = %v\n", in.Path)
	res := &api.GetPictureReply{OutPicture: []byte{}}
	return res, nil
}
