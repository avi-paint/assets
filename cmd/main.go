package main

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/powerman/structlog"

	"gitlab.com/avi-paint/assets/configs"
	"gitlab.com/avi-paint/assets/pkg/def"
	"gitlab.com/avi-paint/assets/pkg/flags"
)

func main() {
	def.Init()
	cfg, _ := configs.Init()

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGHUP, syscall.SIGINT, syscall.SIGQUIT, syscall.SIGABRT, syscall.SIGTERM)

	switch {
	case cfg.Server.Port <= 0:
		flags.FatalFlagValue("must be > 0", "grpc_service_port", cfg.Server.Port)
	}

	logger := structlog.DefaultLogger.SetLogLevel(structlog.ParseLevel(cfg.Server.LogLevel))

	shutdown, err := RunService(cfg, logger)
	if err != nil {
		logger.Fatal(err)
	}

	<-done
	shutdown()
	os.Exit(0)
}
