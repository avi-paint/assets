package main

import (
	"context"

	"github.com/pkg/errors"
	"github.com/powerman/structlog"
	"google.golang.org/grpc"

	"gitlab.com/avi-paint/assets/configs"
	service2 "gitlab.com/avi-paint/assets/internal/transport"
	"gitlab.com/avi-paint/assets/pkg/concurrent"
	"gitlab.com/avi-paint/assets/pkg/netx"
	"gitlab.com/avi-paint/assets/pkg/serve"
)

type Ctx = context.Context

type service struct {
	cfg    *configs.Config
	srv    *grpc.Server
	logger *structlog.Logger
}

func RunService(cfg *configs.Config, log *structlog.Logger) (context.CancelFunc, error) {

	server := service2.NewAssetsServer()
	s := service{
		cfg:    cfg,
		srv:    server,
		logger: log,
	}

	ctxShutdown, shutdown := context.WithCancel(context.Background())
	err := concurrent.Serve(ctxShutdown, shutdown,
		s.serveGRPC,
	)
	if err != nil {
		return nil, errors.Wrap(err, "starting serve services")
	}

	return shutdown, nil
}

func (s *service) serveGRPC(ctx Ctx) error {
	addr := netx.NewAddr(s.cfg.Server.Host, s.cfg.Server.Port)
	return serve.ServerGRPC(ctx, addr, s.srv)
}
