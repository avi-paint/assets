package main

import (
	"fmt"
	"log"
	"testing"

	api "gitlab.com/avi-paint/assets-proto/go/v1/assets"
	"google.golang.org/grpc"
)

const (
	address = "localhost:8000"
)

func newAssetsClient() api.AssetsServiceClient {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	return api.NewAssetsServiceClient(conn)
}

func TestConnection(t *testing.T) {
	//client := newAssetsClient()
	//picture, err := client.GetPictureByPath(context.Background(), &api.GetPictureRequest{Path: address})
	//if err != nil {
	//	fmt.Printf("%v\n", err)
	//	return
	//}

	fmt.Printf("test is completed")
}
