FROM golang:latest AS builder

COPY . /app/

WORKDIR /app

RUN go build -o /build/assets ./cmd/

CMD ["/build/assets"]